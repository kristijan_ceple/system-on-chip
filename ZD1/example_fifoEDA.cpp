//
// Created by kikyy_99 on 03. 12. 2021..
//

#include <systemc.h>
#include "example_fifoEDA.h"
using namespace sc_dt;

void example_fifoEDA::producer_thread() {
    int unsigned number_of_accesses = 16;

    for(int i = 0; i < number_of_accesses; i++) {
        sc_int32 value(std::rand() % 100);

        cout << "[" << sc_time_stamp() << "] writing to FIFO value: " << value << ", free: " << fifo.num_free() << endl;

        fifo.write(value);

        cout << "[" << sc_time_stamp() << "] wrote to FIFO value: " << value << endl;

        wait(1, SC_NS);
    };
};

void example_fifoEDA::consumer_thread() {
    sc_int<32> value(0);

    for(;;) {
        wait(4, SC_NS);

        fifo.read(value);

        cout << "[" << sc_time_stamp() << "] read from FIFO value: " << value << endl;
    };
};

