#include "systemc.h"
#include "example_fifoEDA.h"

using namespace sc_dt;

int sc_main_1(int, char* []) {

    //create the instance of the example_fifoEDA
    example_fifoEDA example_inst("example_inst");

    sc_start();

    if(not sc_end_of_simulation_invoked()) {
        sc_stop;
    }

    return 0;
}