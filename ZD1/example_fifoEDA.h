#ifndef EXAMPLE_H
#define EXAMPLE_H
using namespace sc_dt;

typedef sc_int<32> sc_int32;

SC_MODULE(example_fifoEDA) {

        public:

        sc_fifo<sc_int32> fifo;

        //producer thread
        void producer_thread();

        //consumer thread
        void consumer_thread();

        SC_CTOR(example_fifoEDA) : fifo(2) {
            SC_THREAD(producer_thread);
            SC_THREAD(consumer_thread);
        };

};

#endif